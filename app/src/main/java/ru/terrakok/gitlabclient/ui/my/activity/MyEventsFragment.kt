package ru.terrakok.gitlabclient.ui.my.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_my_activity.*
import kotlinx.android.synthetic.main.layout_base_list.*
import kotlinx.android.synthetic.main.layout_zero.*
import ru.terrakok.gitlabclient.R
import ru.terrakok.gitlabclient.entity.app.target.TargetHeader
import ru.terrakok.gitlabclient.extension.visible
import ru.terrakok.gitlabclient.presentation.my.events.MyEventsPresenter
import ru.terrakok.gitlabclient.presentation.my.events.MyEventsView
import ru.terrakok.gitlabclient.toothpick.DI
import ru.terrakok.gitlabclient.ui.global.BaseFragment
import ru.terrakok.gitlabclient.ui.global.ZeroViewHolder
import ru.terrakok.gitlabclient.ui.my.TargetsAdapter
import toothpick.Toothpick

/**
 * @author Konstantin Tskhovrebov (aka terrakok). Date: 13.06.17
 */
class MyEventsFragment : BaseFragment(), MyEventsView {
    override val layoutRes = R.layout.fragment_my_activity

    @InjectPresenter lateinit var presenter: MyEventsPresenter

    private val adapter: TargetsAdapter by lazy {
        TargetsAdapter(
                { presenter.onUserClick(it) },
                { presenter.onItemClick(it) },
                { presenter.loadNextEventsPage() }
        )
    }
    private var zeroViewHolder: ZeroViewHolder? = null

    @ProvidePresenter
    fun providePresenter(): MyEventsPresenter =
            Toothpick
                    .openScope(DI.MAIN_ACTIVITY_SCOPE)
                    .getInstance(MyEventsPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = this@MyEventsFragment.adapter
        }

        swipeToRefresh.setOnRefreshListener { presenter.refreshEvents() }
        toolbar.setNavigationOnClickListener { presenter.onMenuClick() }
        zeroViewHolder = ZeroViewHolder(zeroLayout, { presenter.refreshEvents() })
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeToRefresh.post { swipeToRefresh.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)

        //trick for disable and hide swipeToRefresh on fullscreen progress
        swipeToRefresh.visible(!show)
        swipeToRefresh.post { swipeToRefresh.isRefreshing = false }
    }

    override fun showPageProgress(show: Boolean) {
        recyclerView.post { adapter.showProgress(show) }
    }

    override fun showEmptyView(show: Boolean) {
        if (show) zeroViewHolder?.showEmptyData()
        else zeroViewHolder?.hide()
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
    }

    override fun showEvents(show: Boolean, events: List<TargetHeader>) {
        recyclerView.visible(show)
        recyclerView.post { adapter.setData(events) }
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }
}